import { Component, OnInit, Input } from '@angular/core';
import { product } from '../shared/model';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  @Input('productAdded') products!: product[];

  showCartForm = false;
  showMessForm = false;

  constructor(private formBuilder: FormBuilder) {
  }

  checkoutForm = this.formBuilder.group({
    name: ['', Validators.required],
    phone: ['', Validators.required],
    address: ['', Validators.required]
  });

  ngOnInit(): void {
  }

  updateQuantity(id: any, quantity: any) {
    this.products.forEach((element) => {
      if (element.id == id) {
        element.quantity = quantity;
      }
    });
  }

  deleteProduct(item: any) {
    const index = this.products.indexOf(item);
    if (index > -1) {
      this.products.splice(index, 1);
    }
  }

  totalCounts(data: product[]) {
    let total = 0;
    data.forEach((item) => {
      total += ((100 - item.promotionPrice) * item.price * item.quantity) / 100;
    }); 
    return total;
  } 

  onSubmit() {
    alert("Successed");   
  } 

  get name() {
    return this.checkoutForm.get('name');
  }

  get phone() {
    return this.checkoutForm.get('phone');
  }

  get address() {
    return this.checkoutForm.get('address');
  }

}
