export interface product{
    id: string,
    productName: string,
    quantity: number,
    image: string,
    price: number,
    promotionPrice: number,
}