import { Injectable } from '@angular/core';
import{HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { product } from './model';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private apiUrl = environment.apiURL;
  constructor(private httpClient:HttpClient) { 

  }

  //return Observable<Object>
  // getProduct(){
  //   const apiUrl='https://steelsoftware.azurewebsites.net/api/FresherFPT';
  //   return this.httpClient.get(apiUrl);
  // }

  getProducts():Observable<HttpResponse<product[]>>{ 
    const options={
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    
    return this.httpClient.get<product[]>(this.apiUrl+'/api/FresherFPT',options);
  }

  // deleteProduct(id:string){
  //   const apiUrl=`https://steelsoftware.azurewebsites.net/api/FresherFPT/${id}`;
  //   return this.httpClient.delete('apiUrl', {observe:'response'});
  // }
}
